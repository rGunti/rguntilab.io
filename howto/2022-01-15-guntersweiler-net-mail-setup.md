---
layout: home
title: 'Intern: guntersweiler.net Email Setup'
permalink: /howto/email-setup-de
date: 2022-01-15 20:18
category: Internal How-To
author: Raphael Guntersweiler
tags: [email,de,howto]
summary: 
---

Dieser Artikel beschreibt die Einrichtung der neuen Mailboxen für `@guntersweiler.net`.

{% include toc.md %}

<div class="alert info">
<div class="title">
&#8505;&#65039; Abschaltung der Mailboxen bei Infomaniak
</div>
<div class="content">
Die Mailboxen bei Infomaniak werden zum 17. Februar 2022 abgeschaltet. Bis dahin kannst du
dich weiterhin auch bei Infomaniak anmelden, aber du wirst dort keine neuen Emails erhalten.
Deine neue Mailbox wird bis zur Abschaltung regelmässig gespiegelt, so sollten keine Emails
verloren gehen.
</div>
</div>

# Links
- [guntiCloud Mail][guntiCloudMail]: Hier kannst du z.B. dein Passwort ändern.
- [Webmail](https://mail.guntersweiler.net)

# Vorbereitung
## Passwort ändern
Du erhältst ein neues, temporäres Passwort. Um alle Funktionen freizuschalten, musst du das Passwort ändern.

Gehe dazu auf [guntiCloud Mail][guntiCloudMail] und melde dich mit deiner
`@guntersweiler.net` Mailadresse und deinem Passwort an.

![guntiCloud Mail Login](/assets/imgs/mail-login.png)

Klicke dann auf "Passwort ändern" und vergebe ein neues Passwort.

![guntCloud Mail Passwort ändern](/assets/imgs/mail-changepw.png)

# Enrichtung
## iPhone / iPad
Entferne vor der Einrichtung das bestehende Infomaniak-Email-Konto von deinem Gerät, falls es noch besteht.
Du findest sie unter `Einstellungen` > `Mail` > `Accounts`.

Du kannst zwischen folgenden Varianten wählen: Automatische Einrichtung oder Manuelle Einrichtung mit Exchange.

### Automatische Einrichtung
1. Öffne [guntiCloud Mail][guntiCloudMail] in Safari und melde dich an.
2. Scrolle runter zu `Apple-Verbindungsprofil mit App-Passwort` und tippe auf `E-Mail, Kalender und Adressbücher`.<br>
   ![](/assets/imgs/guntiCloud Mail.png)
3. Bestätige die folgende Abfrage mit `Zulassen`.<br>
   ![](/assets/imgs/guntiCloud Mail 2.png)
4. Wechsle in die App `Einstellungen`. Du findest dort oben unter deiner Apple ID einen neuen Eintrag `Profil geladen`.
   Tippe darauf und installiere das Profil mit `Installieren`. Bestätige ausserdem die zusätzliche Abfrage, dass das
   Profil nicht signiert ist.
5. Wechsle in die Mail-App. Deine neue Mailbox sollte nun angezeigt werden.

### Manuelle Einrichtung mit Exchange
1. Öffne [guntiCloud Mail][guntiCloudMail] in Safari und melde dich an.
2. Tippe auf das Menü "Allgemein" und wähle dann "App-Passwörter" aus.
3. Tippe auf "Erstelle App-Passwort".
4. Gib einen Namen für dein Gerät ein (z.B. "iPhone").
5. Tippe auf "Generieren" unter "Passwort", um dir ein Passwort zu erzeugen. Kopiere dir das angezeigte Passwort
   anschliessend. Du wirst später keine Möglichkeit mehr haben, dieses Passwort auszulesen.
6. Tippe anschliessend unten auf "Hinzufügen".
7. Öffne die App `Einstellungen` und navigiere zu `Mail` > `Accounts`.
8. Wähle `Account hinzufügen`.
9. Wähle aus der Liste `Microsoft Exchange`.
10. Gibt deine Email-Adresse und einen Namen für deine Mailbox ein (bspw. `guntersweiler.net`). Wähle dann `Weiter`.
11. Wenn du gefragt wirst, gibt das kopierte App-Passwort ein und bestätige die Eingabe.

## PC
Bevor du deinen Mail-Client einrichtest, erstelle ein App-Passwort:

1. Öffne [guntiCloud Mail][guntiCloudMail] in deinem Browser und melde dich an.
2. Wähle "App-Passwörter" und dann "Erstelle App-Passwort".
3. Gib einen Namen für dein Gerät ein (z.B. "Laptop Thunderbird").
4. Klippe auf "Generieren" unter "Passwort", um dir ein Passwort zu erzeugen. Kopiere dir das angezeigte Passwort
   anschliessend. Du wirst später keine Möglichkeit mehr haben, dieses Passwort auszulesen.
5. Klicke anschliessend unten auf "Hinzufügen".

### Mozilla Thunderbird
1. In Thunderbird, wähle im Menü (oben rechts) `Konten-Einstellungen`.
2. Klicke dann unten links auf das Menü `Konten-Aktionen` > `E-Mail-Konto hinzufügen...`.
3. Trage deinen Namen, deine Email-Adresse und dein zuvor kopiertes App-Passwort ein.
   Stelle ausserdem sicher, dass `Passwort speichern` aktiviert ist.
4. Klicke anschliessend auf `Weiter`.
5. In der folgenden Anzeige solltest du den Punkt `IMAP` aktiviert haben. Klicke anschliessend auf `Fertig`.
6. Anschliessend wird dir die Einrichtung für das Adressbuch und den Kalender vorgeschlagen. Du wirst eine
   Warnung mit folgender Nachricht sehen:<br>
   ![Sicherheits-Ausnahmeregel hinzufügen](/assets/imgs/mail-thunderbird-caldav.png)<br>
   Stelle sicher, dass im Feld `Adresse:` die folgende Adresse verzeichnet ist: `gunti.cloud`.
   Bestätige anschliessend mit `Sicherheits-Ausnahmeregel bestätigen`.
7. Klicke auf die Punkte `Adressbücher` und `Kalender` und wähle bei den einzelnen Unterpunkten `Verbinden`.
8. Bestätige anschliessend mit `Beenden`.

### Windows Mail
1. Öffne die `Mail`-App und klicke unten links auf die `Einstellungen` (Zahnrad-Symbol).
2. Wähle im Menü rechts den Punkt `Konten verwalten` > `Konto hinzufügen`.
3. Wähle im nächsten Dialog `Office 365, Exchange` aus.
4. Trage deine Email-Adresse ein und klicke auf `Weiter`.
5. Füge dein zuvor kopiertes App-Passwort ein und wähle `Weiter`.
6. Wähle anschliessend `Fertig`.

### Andere Email-Clients
Falls dein Email-Client hier nicht aufgeführt ist, findest du weitere Dokumentation in [guntiCloud Mail][guntiCloudMail]
unter dem Punkt `Konfigurationsanleitungen für E-Mail-Programme und Smartphones anzeigen`.

![Konfigurationsanleitungen für E-Mail-Programme und Smartphones anzeigen](/assets/imgs/mail-config-help.png)

[guntiCloudMail]: https://mail.gunti.cloud/
