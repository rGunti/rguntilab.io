# ################################################# #
# DOCKERFILE for building raphael.guntersweiler.net #
# ################################################# #

FROM jekyll/jekyll:4 AS build
RUN apk update
RUN apk add graphviz
RUN apk add gettext
