const THEMES = {
    default: 'theme--default',
    dark: 'theme--dark'
};
var CURRENT_THEME = null;
var HTML = document.getElementsByTagName('html')[0];

function getStoredTheme() {
    if (supportsLocalStorage()) {
        return window.localStorage.getItem('theme') || 'default';
    }
    return 'default';
}
function setStoredTheme(theme) {
    if (supportsLocalStorage()) {
        window.localStorage.setItem('theme', theme);
    }
}

function supportsLocalStorage() {
    return typeof window.localStorage !== 'undefined';
}

function setTheme(theme, store) {
    CURRENT_THEME = theme;
    HTML.className = THEMES[theme];
    if (store) {
        setStoredTheme(theme);
    }
}

function isDarkTheme() {
    return CURRENT_THEME === 'dark';
}
function setDarkTheme(isDark) {
    setTheme(isDark ? 'dark' : 'default', true);
}

function switchThemeFn(e) {
    return function(ev) {
        const newTheme = !isDarkTheme();
        setDarkTheme(newTheme);
    };
}

function onDocumentLoaded() {
    try {
        document.querySelectorAll('.theme-switcher').forEach(function(i) {
            if (supportsLocalStorage()) {
                i.classList.remove('hidden');
                i.addEventListener('click', switchThemeFn(i));
            }
        });
    } catch (e) { /* ignored */ }
}

function main() {
    // Load Theme
    const currentTheme = getStoredTheme();
    if (currentTheme) {
        CURRENT_THEME = currentTheme;
        setTheme(currentTheme, false);
    }

    window.addEventListener('load', function() {
        onDocumentLoaded();
    }, false);
}

main();
