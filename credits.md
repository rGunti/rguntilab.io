---
layout: default
title: Credits
---

On this page you will find references to third-party code and technology I've used to build
this website.

- [**Jekyll**][jekyllrb]{:target="_blank"} Website Generator
- [**Simple-Jekyll-Search**][simple-jekyll-search]{:target="_blank"} by Christian Fei, 
  licensed under the [MIT License][simple-jekyll-search-license]{:target="_blank"}<br>
  _Provides Search function on this website_
- [**pygments-css**][pygments-css]{:target="_blank"}, licensed under the [UNLICENSE][pygments-license]{:target="_blank"}<br>
  _Provides decent syntax highlighting for code blocks_

[jekyllrb]: https://jekyllrb.com/
[simple-jekyll-search]: https://github.com/christian-fei/Simple-Jekyll-Search
[pygments-css]: https://github.com/richleland/pygments-css

[simple-jekyll-search-license]: https://github.com/christian-fei/Simple-Jekyll-Search/blob/master/LICENSE.md
[pygments-license]: https://github.com/richleland/pygments-css/blob/master/UNLICENSE.txt
