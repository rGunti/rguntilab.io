---
layout: post
title: 'CarPi: (Re-)Introduction'
date: 2022-01-24 00:00
category: CarPi
author: Raphael Guntersweiler
tags: ['carpi']
summary: The CarPi project introduction post
---

When I got my first car, I was always interested in its fuel consumption.
Unfortunately, my car did not come with a fuel consumption display so all I had
at first was an Excel sheet where I entered the amount of petrol I last got,
the odometer or tripmeter value and the cost per kilometer. And it was alright
at first. When I learned that virtually all cars have a diagnostic port for
reading out various stats, including all the required parameters to read out
your current fuel consumption, I was hooked and started reading into how this
works and how I could leverage that to build my own fuel consumption gauge.
Thus, my years-long project which I named "**CarPi**" was born.

<!--more-->

On my previous WordPress blog (which doesn't exist anymore) I already wrote
quite a few times about it, although this was in German. To not loose all the
knowledge I built over the years, I'm starting to document everything in short-
to medium-sized blog posts on this website. This here post serves as an
introduction into the series. You will find all these posts under the category
[CarPi](/posts/categories.html#c_CarPi).
