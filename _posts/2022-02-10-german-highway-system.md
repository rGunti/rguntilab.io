---
layout: post
title: The German Highway System
date: 2022-02-10 19:00
category: off-topic
author: Raphael Guntersweiler
tags: [off-topic,highways,roadways,germany]
summary: 
---

Prompted by a video by the YouTube creator [CGPGrey][yt-cgpgrey] about the
[US highway system][highway-video], mainly centered around its numbering scheme,
I was inspired to begin working on a similar project centered around Highway
systems from other countries, starting with the countries I am most familiar
with, first being Germany. My plan is to eventually create an animated video
presenting this in a graphically more interesting way.

<!--more-->

{% include toc.md %}

## Numbering Scheme & Signing
German Highways are assigned a number, ranging from one to three digits,
prefixed by the letter `A` (for "Autobahn"). Officially, they are called
"Bundesautobahn" (short "BAB", for "_Federal Highway_" or "_Federal Motorway_").
The prefix is however not denoted on signs. Highway signs feature a white number
on a blue background and a hexagonal white border.

<div class="fig">
<img class="max100"
     src="/assets/imgs/highways/de/A42.svg"
     alt="Sign for the `A42` highway, ranging from Kamp-Linfort to Dortmund" />
<p>
Sign for the <code>A42</code> highway, ranging from Kamp-Linfort to Dortmund
</p>
</div>

The number of the highway usually designates its importance and cardinal
direction. Single digit numbers denote highways which reach across large
parts or the whole country, double-digits are usually traveling within state
borders or only cross into neighboring states, three digits are reserved for
regional or city-local roads, like bypasses or access roads to larger highways.

Highways with even numbers usually travers the country in a vertical (meaning
North-to-South) way whereas odd numbers usually run horizontally (East-to-West).

For highways with two or three digits, the first digits is defined based on the
larger region it is located in.

## Noteworthy Highways

### Longest single-digit highway
The longest cross-country highway is the `A7` ranging from the border city of
Flensburg bordering Denmark's `E45` in the North to the Bavarian city of Füssen
bordering Austria and it's `B179`. The `A7` covers a distance of 962 km
(598 mi). `A7` is also part of the European Route network being part of the
5190 km (3224 mi) long `E45` (ranging from Alta, Norway to Gela, Italy) as well
as `E43`, `E532` and the 8778 km (5452 mi) long `E40`, ranging from Calais,
France all the way to Ridder, Kasachstan.

### Shortest single-digit highway
The shortest single-digit highway is the `A5`, covering 440 km (273 mi) from
Niederaula in Hesse to the border city of Weil am Rhein, connecting
to the Swiss `A2`/`E35` to Basel.

### Gaps
Various highways, even single-digit ones, have larger gaps in them, meaning they
end in one spot and start again later.

#### A4
For example, the `A4` starts in the West in Aachen, bordering the Dutch `A76`
to Heerlen, but then ends in Krombach, restarting at the "Kirchheimer Dreieck",
which branches off the `A7`, and then continuing all the way to the Polish
border, ending in the German-Polish town of Görlitz / Zgorzelec and connecting
to the Polish `A4` connecting to Wrocław. The `A4` was also one of the Transit
routes traversing the DDR (_GDR_) before the German reunification.

The `A8` starts from the Austrian border near Salzburg, connecting from the
Austrian `A1` in Bad Reichenhall, and stops in München (_Munich_), entering in
the ring road `B2R`. It continues on the other side of the city at Schloss
Blutenburg, without being connected to the ring road, ranging over Stuttgart to
Karlsruhe, where it ends for a second time, connecting to the `A5`. The last leg
of `A8` starts in Primasens, connecting to `A62` in the north, following the
French border and reaches to the Luxembourgian border connecting to `A13` near
the town of Schengen.

#### A98
Another remarkable highway is `A98`, as it stretches over only 47 km (29 mi),
another 34 km (21 mi) are currently being planned (as of February 2021).
The first segment starts near the southern end of `A5`, branching off in
Weil am Rhein and ending in Rheinfelden, ranging only about 17.8 km (11 mi).
The next section is a relabelled 3-lane road providing a bypass to villages
along the Rhein river starting after Bad Säckingen and ending after 10 km
(6.2 mi) short after Luttingen. This section is also co-labelled as `B34` and
`E54` which continues further.

The third section provides a bypass for the village of Waldshut-Tiengen, ending
again after only 7.3 km (4.5 mi) in Lauchringen, rejoining the `B34`. This leg
passes partially under the village, providing mostly two lanes.

The last leg of `A98` starts at Kreuz Hegau, branching off the `A81` and ending
in Stockach near Lake of Constance after about 12.7 km (7.9 mi) merging into
the `B31n` and `B31` which follows the lake south.

### Shortest labelled and operational BAB
The shortest, as "Bundesautobahn" labelled and operational highway in Germany
is the `A831` in Stuttgart, connecting the Bundesstraße `B14` with the `A8` and
`A81`. It also only has one exit, "(1) Stuttgart-Vaihingen" before ending in
the "(2) Kreuz Stuttgart".

Other shorter highways exist, only having a length of about 1 km (0.6 mi) but
these were officially relabelled as mostly branches of existing larger highways.
Some examples are the `A102`, being merged with the `A100` Berlin city
ring, and the `A105`, being merged with the `A111` connecting to the `A10`
outside of Berlin.

### A999 / A99
The highest number for a German highway, `A999` has been assigned to a
highway project in München. `A999` was planned to be a highway ring  which has
however not implemented. Parts of it was executed under the `B2R` ring road.
A highway ring was implemented with the `A99`.

The `A99` ring has never been fully completed. It currently spans about two
thirds around the city, starting in the west branching off the `A96`, crossing
`A8`, `A92`, `A9`, `A94` and ending at a crossing between `A8` and `A995`, which
heads back into the city center.

## Trivia
Germany only has one direct highway connection to Denmark, that being the `A7`
in Flensburg. The other highway connection, `A1`, ends in Heiligenhafen and 
continues as `E47`/`B207` onto the German island of Fehmarn in the Baltic Sea.
`E47` connects to the Danish island of Lolland via ferry.

## Differences to other countries
The US practise of referring to a highway / interstate with its number and a
cardinal direction (i.e. `I-40 S` for _Interstate 40_, southbound) is uncommon
in Germany. It is common to refer to them via their number and one of the major
cities en route. For example, a person traveling on the `A7` northbound will
refer to it as "`A7`, Hanover to Hamburg". This notation is also used in traffic
announcement broadcasts, giving travelers the highway, direction and general
location of an incident. In more local settings, exit names are also added.

> `A560`, Hennef towards Saint Augustin, between Niederpleis and Siegburg,
> a spade is lying on the road. [...]

<div class="fig">
<img src="/assets/imgs/highways/de/gmaps-sample.png"
     alt="Markings on a screenshot of Google Maps, denoting the area of danger"
     />
<p>
Markings on a screenshot of Google Maps, denoting the area of danger
</p>
</div>

In this example, the radio broadcast warns us that a spade has been found lying
on the highway, posing a threat to drivers. It has been found between the
two marked exits on the westbound route.

> `A2`, Hannover towards Dortmund, between Bad Eisen and cross Bad Oeynhausen
> more than 10 km [traffic].
>
> [...]
>
> `A3`, Arnhem towards Cologne, between cross Oberhausen and cross Kaiserberg
> 10 km [traffic], more than 20 minutes.

These two examples warn drivers about traffic jams and estimated delays when
traversing this section.

_sample transcript from a German radio broadcast, translated from German,_
_[Source][transcript-source]_

## Sources
Most of this article has been created based of the
[List of autobahns in Germany][list-wiki-de]
([English version][list-wiki-en]) Wikipedia article and its references, as well
as public map data.

## Future Plans
Next countries on my list to cover are
- Switzerland
- Austria
- Netherlands
- Denmark
- Sweden
- Norway

as well as the **European Routes**, since they will be mentioned again.

[yt-cgpgrey]: https://www.youtube.com/greymatter
[highway-video]: https://www.youtube.com/watch?v=8Fn_30AD7Pk
[list-wiki-en]: https://en.wikipedia.org/wiki/List_of_autobahns_in_Germany
[list-wiki-de]: https://de.wikipedia.org/wiki/Liste_der_Bundesautobahnen_in_Deutschland
[transcript-source]: https://youtu.be/kDeI4I_sWZM?t=361
