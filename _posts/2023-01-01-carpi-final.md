---
layout: post
title: CarPi
date: 2023-01-01 00:00
category: CarPi
author: Raphael Guntersweiler
tags: [carpi,raspberry pi]
summary: This post shows the state of CarPi and reports about its current achievements and issues.
---

It has been almost a year since I've written about CarPi. Partially because I keep forgetting to invest time into writing down everything, partially because I'm busy with a lot of other projects. But with this post, I want to write down what the state of CarPi is, what works and what doesn't work.

<!--more-->

{% include toc.md %}

## Software Stack

The software stack has changed a bit to improve performance. The Node.js server component has been replaced with a dotnet core server application. This makes deployment a lot easier since Node and npm are no longer required. A simple executable and a few additional supporting files are enough to make them run, even if the dotnet runtime has to be installed first.

Additionally, a recorder and simulator have been created to help document generated data and to reuse it for development.
All this software has been released on GitLab in the [CarPi project][carpi-src].

## What does work

The biggest test for CarPi was my road trip to Scandinavia in Spring 2022. So after over 3,500 kilometers, I can report that data fetching and processing works reliably. Additionally, the web app also works quite well and all trips were spotlessly recorded on a USB stick and can be replayed at will.

## What doesn't work

Live reporting the location has been quite a problem. It was my original intention to submit my location and the car's state of charge in regular intervals to [Phonetrack][phonetrack] hosted on my [Nextcloud][nextcloud] instance. The internet connection should been fine as I have an LTE dongle attached and the data I'm transferring is rather small, being only a few kilobytes every other minute or so. However it wasn't that easy. Network coverage is pretty much one issue, however it seems like establishing the connection and sending the data ran into so many issues which I could not diagnose when riding down a highway at 100+ kilometers an hour, I eventually stopped caring about the reporting feature.

There is also still the problem of power. I am using a Y-splitter attached to the car's OBD-II port to power the Raspberry Pi and using an OBD-USB adapter to communicate with the car. The power adapter I'm using even has a switch that turns off if the car's 12V battery falls below a certain threshold. My car doesn't seem to be aware of that limit though. If I keep the adapter connected for too long when parked, my car will turn off the power on the OBD-II port and apparently also to the rest of the car's indoor controls. This means when I enter the car, the trip meter and a few other settings are reset. Of course, this is an issue with my car and not with CarPi directly, but unfortunately I can't really work around this. If I run CarPi from a battery bank instead, this problem still occurs, but only much later.

## Future of CarPi

Lastly I want to share a word about the future of CarPi. 2023 will be the year I exchange my current car for a new one, which means I will have to redevelop the OBD-II communication since it will be of a different make. To be honest I am not yet sure if I will do that. We will see.

[carpi-src]: https://gitlab.com/car-pi
[nextcloud]: https://nextcloud.com/
[phonetrack]: https://apps.nextcloud.com/apps/phonetrack
