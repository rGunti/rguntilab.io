---
layout: home
title: Website Manifesto - My Goals for this Website
---

On this page, I would like to highlight a few key points I want to accomplish with this website and invite anyone
to join in and help me improve it by submitting suggestions.

# Simplicity
One of the most important aspects I had in mind developing this website was Simplicity.
That means: Simple HTML pages, simple styling, simple JavaScript. Therefore this website
doesn't use any external UI libraries or frameworks, such as Bootstrap, Angular, React or the like.

# Accessibility
I am privileged enough to live in a first-world country with decent internet access and without any
disabilities. But not everyone is that lucky.

This website should be accessible to everyone. Through Simplicity, I strive to keep my sites'
bandwidth and system requirements low and make it accessible for people who rely on assistive technologies.
If you know how to improve and test this website for accessibility, please reach out to me.

# Open Source
This website is generated using Jekyll. The source code for this site is stored on GitLab, which means
you're free to checkout the source code yourself and use it to create your own blog.

If you have suggestions for improvements, you may of course also submit a Pull / Merge Request with changes.

Keep in mind that although I don't rely on other bigger UI libraries or frameworks, I use the following components
to provide additional functionality:

- `Simple-Jekyll-Search` by _Christian Fei_ (licensed under the MIT License)
  for the **Search** feature (source file is located here: `/assets/search.js`)

# Privacy
Without any external (code) dependencies and no advertisement, privacy is obviously improved a lot. I don't want
to include services like Google AdSense or similar. Obviously this also means I won't earn money from it but
this is also not my intent with this site.

Keep in mind though that this website is hosted through GitLab Pages so whatever they decide to log is out of
my control.

# Maintenance
Maintaining a website is quite a bit of work. I've hosted WordPress blogs before and while easy, they require their
own infrastructure to run and maintain. This website aims to cut that all. It's custom built using a website generator.
This means that all this website is, is a set of HTML pages with links. It doesn't have features like Logins, comments,
likes, etc. That is by design. No comment section means I don't have to monitor comments for spam or harmful content.

This doesn't mean that there is no way to comment. The following ways are available for commenting:
- Email
- Social Media (Twitter)
- GitLab Issues (_yes, if you have a comment, you may submit an issue on GitLab to this project_)

# Compatibility
I am very interested in Retro Tech. And while old computers don't really belong on the internet anymore, having them
able to display my website is still something I very much would like to see. That's why I regularly test my website
with older browsers and operating systems. While I can't guarantee that this site works 100% of the time, I would like to make
the content available in most browsers, modern or legacy, graphical or text-based.

Here are a few of my benchmarks I try:
- [Lynx](https://lynx.invisible-island.net/), a text-based web browser available 
- Internet Explorer 6
- Mozilla Firefox 2

A more thorough list can be found here: [Compatibility Report][compatibility]

[compatibility]: {% link compatibility-report.html %}
